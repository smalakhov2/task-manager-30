package ru.malakhov.tm.exception.unknown;

import ru.malakhov.tm.exception.AbstractException;

public final class UnknownUserException extends AbstractException {

    public UnknownUserException() {
        super("Error! Unknown user ...");
    }

}
