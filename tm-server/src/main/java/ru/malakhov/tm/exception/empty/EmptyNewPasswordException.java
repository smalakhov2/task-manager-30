package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public final class EmptyNewPasswordException extends AbstractException {

    public EmptyNewPasswordException() {
        super("Error! New password is empty...");
    }

}
