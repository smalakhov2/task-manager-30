package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.ISqlSessionProvider;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.exception.unknown.UnknownUserException;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.repository.UserRepository;
import ru.malakhov.tm.util.HashUtil;

import java.util.List;

public final class UserService extends AbstractService<UserDto, IUserRepository> implements IUserService {

    public UserService(@NotNull final ISqlSessionProvider sqlSessionProvider) {
        super(sqlSessionProvider);
    }

    @Override
    public @NotNull IUserRepository getRepository() {
        return new UserRepository(getEntityManager());
    }
    
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @NotNull final UserDto user = new UserDto(login, password, "");
        persist(user);
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        @Nullable final UserDto user = new UserDto(login, password, email);
        persist(user);
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final UserDto user = new UserDto(login, password, "");
        user.setRole(role);
        persist(user);
    }

    @NotNull
    @Override
    public List<UserDto> findAllDto() {
        @NotNull final IUserRepository repository = getRepository();
        return repository.findAllDto();
    }

    @NotNull
    @Override
    public List<User> findAllEntity() {
        @NotNull final IUserRepository repository = getRepository();
        return repository.findAllEntity();
    }

    @Nullable
    @Override
    public UserDto findOneDtoById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        @NotNull final IUserRepository repository = getRepository();
        return repository.findOneDtoById(id);
    }

    @Nullable
    @Override
    public User findOneEntityById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        @NotNull final IUserRepository repository = getRepository();
        return repository.findOneEntityById(id);
    }

    @Nullable
    @Override
    public UserDto findOneDtoByLogin(
            @Nullable final String login
    ) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        
        @NotNull final IUserRepository repository = getRepository();
        return repository.findOneDtoByLogin(login);
    }

    @Nullable
    @Override
    public User findOneEntityByLogin(
            @Nullable final String login
    ) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        
        @NotNull final IUserRepository repository = getRepository();
        return repository.findOneEntityByLogin(login);
    }

    @Override
    public void removeAll() {
        @NotNull final IUserRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll();
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IUserRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneByLogin(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @NotNull final IUserRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneByLogin(login);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String password,
            @Nullable final String newPassword
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();

        @Nullable final UserDto user = findOneDtoById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (!user.getPasswordHash().equals(passwordHash)) throw new AccessDeniedException();
        @Nullable final String newPasswordHash = HashUtil.salt(newPassword);
        if (newPasswordHash == null) throw new AccessDeniedException();
        user.setPasswordHash(newPasswordHash);
        merge(user);
    }

    @Override
    public void updateUserInfo(
            @Nullable final String userId,
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        @Nullable final UserDto user = findOneDtoById(userId);
        if (user == null) throw new AccessDeniedException();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        merge(user);
    }

    @Override
    public void lockUserByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDto user = findOneDtoByLogin(login);
        if (user == null) throw new UnknownUserException();
        user.setLocked(true);
        merge(user);
    }

    @Override
    public void unlockUserByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDto user = findOneDtoByLogin(login);
        if (user == null) throw new UnknownUserException();
        user.setLocked(false);
        merge(user);
    }

}
