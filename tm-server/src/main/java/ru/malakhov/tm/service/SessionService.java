package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.ISqlSessionProvider;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyLoginException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.unknown.UnknownUserException;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.repository.SessionRepository;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.util.SignatureUtil;

import java.util.List;

public final class SessionService extends AbstractService<SessionDto, ISessionRepository> implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final IServiceLocator serviceLocator,
            @NotNull final ISqlSessionProvider sqlSessionProvider) {
        super(sqlSessionProvider);
        this.serviceLocator = serviceLocator;
    }
    
    @Override
    public @NotNull ISessionRepository getRepository() {
        return new SessionRepository(getEntityManager());
    }

    @NotNull
    @Override
    public List<SessionDto> findAllDto() {
        @NotNull final ISessionRepository repository = getRepository();
        return repository.findAllDto();
    }

    @NotNull
    @Override
    public List<Session> findAllEntity() {
        @NotNull final ISessionRepository repository = getRepository();
        return repository.findAllEntity();
    }

    @NotNull
    public List<SessionDto> findAllDtoByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        
        @NotNull final ISessionRepository repository = getRepository();
        return repository.findAllDtoByUserId(userId);
    }

    @NotNull
    public List<Session> findAllEntityByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        
        @NotNull final ISessionRepository repository = getRepository();
        return repository.findAllEntityByUserId(userId);
    }

    @Nullable
    @Override
    public SessionDto findOneDtoById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        @NotNull final ISessionRepository repository = getRepository();
        return repository.findOneDtoById(id);
    }

    @Nullable
    @Override
    public Session findOneEntityById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        @NotNull final ISessionRepository repository = getRepository();
        return repository.findOneEntityById(id);
    }

    @Override
    public void removeAll() {
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll();
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAllByUserId(userId);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public UserDto checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;

        @Nullable final UserDto user = serviceLocator.getUserService().findOneDtoByLogin(login);
        if (user == null) return null;
        if (user.getLocked()) return null;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        if (passwordHash.equals(user.getPasswordHash())) return user;
        return null;
    }

    @Nullable
    @Override
    public SessionDto open(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        @Nullable final UserDto user = checkDataAccess(login, password);
        if (user == null) return null;
        @NotNull final SessionDto session = new SessionDto(System.currentTimeMillis(), user.getId());
        @Nullable final SessionDto signSession = setSignature(session);
        persist(signSession);
        return signSession;
    }

    @Nullable
    @Override
    public SessionDto setSignature(@Nullable final SessionDto session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable String signature = SignatureUtil.sign(session, salt, cycle);
        if (signature == null) return null;
        session.setSignature(signature);
        return session;
    }

    @Nullable
    @Override
    public UserDto getUser(@Nullable final SessionDto session) throws AbstractException {
        validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getUserService().findOneDtoById(userId);
    }

    @NotNull
    @Override
    public String getUserId(@Nullable final SessionDto session) throws AccessDeniedException {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<SessionDto> getListSession(@Nullable final SessionDto session) throws AbstractException {
        validate(session);
        return findAllDtoByUserId(session.getUserId());
    }

    @Override
    public void close(@Nullable final SessionDto session) throws AccessDeniedException {
        validate(session);
        removeOne(session);
    }

    @Override
    public void closeAll(@Nullable final SessionDto session) throws AccessDeniedException {
        validate(session);
        removeAll();
    }

    @Override
    public void validate(@Nullable final SessionDto session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();

        @Nullable final SessionDto tempSession = session.clone();
        if (tempSession == null) throw new AccessDeniedException();
        @Nullable final SessionDto signTempSession = setSignature(tempSession);
        if (signTempSession == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = signTempSession.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        try {
            @Nullable final SessionDto sessionInStorage = findOneDtoById(session.getId());
            if (sessionInStorage == null) throw new AccessDeniedException();
        } catch (EmptyIdException e) {
            throw new  AccessDeniedException();
        }
    }

    @Override
    public void validate(
            @Nullable final SessionDto session,
            @Nullable final Role role
    ) throws AbstractException {
        if (role == null) throw new AccessDeniedException();
        if (session == null) throw new AccessDeniedException();

        validate(session);
        @NotNull final String userId = session.getUserId();
        @Nullable final UserDto user = serviceLocator.getUserService().findOneDtoById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public boolean isValid(@Nullable final SessionDto session) {
        try {
            validate(session);
            return true;
        } catch (AccessDeniedException e) {
            return false;
        }
    }

    @Override
    public void signOutByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDto user = serviceLocator.getUserService().findOneDtoByLogin(login);
        if (user == null) throw new UnknownUserException();
        @NotNull final String userId = user.getId();
        removeAllByUserId(userId);
    }

}
