package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractRepository<TaskDto> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @NotNull
    @Override
    public List<TaskDto> findAllDto() {
        return em.createQuery("SELECT e FROM TaskDto e", TaskDto.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllEntity() {
        return em.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAllDtoByUserId(@NotNull final String userId) {
        @NotNull final List<TaskDto> tasks =
                em.createQuery("SELECT e FROM TaskDto e WHERE e.userId=:userId", TaskDto.class)
                        .setParameter("userId", userId)
                        .getResultList();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAllEntityByUserId(@NotNull final String userId) {
        @NotNull final List<Task> tasks =
                em.createQuery("SELECT e FROM Task e WHERE e.user.id=:userId", Task.class)
                        .setParameter("userId", userId)
                        .getResultList();
        return tasks;
    }

    @Nullable
    @Override
    public TaskDto findOneDtoById(@NotNull final String id) {
        @NotNull final List<TaskDto> tasks =
                em.createQuery("SELECT e FROM TaskDto e WHERE e.id=:id", TaskDto.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Nullable
    @Override
    public Task findOneEntityById(@NotNull final String id) {
        @NotNull final List<Task> tasks =
                em.createQuery("SELECT e FROM Task e WHERE e.id=:id", Task.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final List<TaskDto> tasks =
                em.createQuery("SELECT e FROM TaskDto e WHERE e.userId=:userId AND e.id=:id", TaskDto.class)
                        .setParameter("userId", userId)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Nullable
    @Override
    public Task findOneEntityById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final List<Task> tasks =
                em.createQuery("SELECT e FROM Task e WHERE e.user.id=:userId AND e.id=:id", Task.class)
                        .setParameter("userId", userId)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<TaskDto> tasks =
                em.createQuery("SELECT e FROM TaskDto e WHERE e.userId=:userId ORDER BY e.name ASC", TaskDto.class)
                        .setParameter("userId", userId)
                        .getResultList();
        if (index >= tasks.size()) return null;
        return tasks.get(index);
    }

    @Nullable
    @Override
    public Task findOneEntityByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<Task> tasks =
                em.createQuery("SELECT e FROM Task e WHERE e.user.id=:userId ORDER BY e.name ASC", Task.class)
                        .setParameter("userId", userId)
                        .getResultList();
        if (index >= tasks.size()) return null;
        return tasks.get(index);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final List<TaskDto> tasks =
                em.createQuery("SELECT e FROM TaskDto e WHERE e.userId=:userId AND e.name=:name", TaskDto.class)
                        .setParameter("userId", userId)
                        .setParameter("name", name)
                        .setMaxResults(1)
                        .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Nullable
    @Override
    public Task findOneEntityByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final List<Task> tasks =
                em.createQuery("SELECT e FROM Task e WHERE e.user.id=:userId AND e.name=:name", Task.class)
                        .setParameter("userId", userId)
                        .setParameter("name", name)
                        .setMaxResults(1)
                        .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Override
    public void removeAll() {
        @NotNull final List<Task> tasks = findAllEntity();
        for (@NotNull final Task task: tasks) em.remove(task);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAllEntityByUserId(userId);
        for (@NotNull final Task task: tasks) em.remove(task);
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final Task task = findOneEntityById(id);
        if (task == null) return;
        em.remove(task);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneEntityById(userId, id);
        if (task == null) return;
        em.remove(task);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneEntityByIndex(userId, index);
        if (task == null) return;
        em.remove(task);
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findOneEntityByName(userId, name);
        if (task == null) return;
        em.remove(task);
    }

}
