package ru.malakhov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.category.UnitCategory;

@Category(UnitCategory.class)
public class HashUtilTest {

    @NotNull
    private final String VALUE = "ASDASDASDAD";

    @Test
    public void testMd5() {
        @Nullable final String incorrectResult = HashUtil.md5(null);
        Assert.assertNull(incorrectResult);

        @Nullable final String result = HashUtil.md5(VALUE);
        Assert.assertNotNull(result);
        @Nullable final String rehashResult = HashUtil.md5(VALUE);
        Assert.assertEquals(result, rehashResult);
        @Nullable final String changedResult = HashUtil.md5(VALUE + "1");
        Assert.assertNotEquals(result, changedResult);
    }

    @Test
    public void testSalt() {
        @Nullable final String incorrectResult = HashUtil.salt(null);
        Assert.assertNull(incorrectResult);

        @Nullable final String result = HashUtil.salt(VALUE);
        Assert.assertNotNull(result);
        @Nullable final String md5Result = HashUtil.md5(VALUE);
        Assert.assertNotEquals(result, md5Result);
        @Nullable final String rehashResult = HashUtil.salt(VALUE);
        Assert.assertEquals(result, rehashResult);
        @Nullable final String changedResult = HashUtil.salt(VALUE + "1");
        Assert.assertNotEquals(result, changedResult);
    }

}
