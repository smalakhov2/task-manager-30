package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.exception.empty.EmptyIdException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class AbstractServiceTest extends AbstractDataTest {

    @NotNull
    private final IProjectService projectService = bootstrap.getProjectService();

    @NotNull
    private final ProjectDto projectOne = new ProjectDto("Project1", "", userDto.getId());

    @NotNull
    private final ProjectDto projectTwo = new ProjectDto("Project2", "", userDto.getId());

    @NotNull
    private final ProjectDto projectThree = new ProjectDto("Project3", "", userDto.getId());

    @NotNull
    private final ProjectDto unknownProject = new ProjectDto("Unknown", "", unknownUserDto.getId());

    @NotNull
    private final List<ProjectDto> allProjects = new ArrayList<>(Arrays.asList(projectOne, projectTwo, projectThree));

    private void loadData() {
        bootstrap.getProjectService().persist(allProjects);
    }

    public AbstractServiceTest() throws Exception {
        super();
    }

    @Before
    public void before(){
        bootstrap.getUserService().persist(userDto, adminDto);
    }

    @After
    public void after() {
        bootstrap.getProjectService().removeAll();
        bootstrap.getUserService().removeOne(userDto);
        bootstrap.getUserService().removeOne(adminDto);
    }

    @Test
    public void testPersist() {
        @Nullable final ProjectDto nullProject = null;
        projectService.persist(nullProject);
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertTrue(projects.isEmpty());

        projectService.persist(projectOne);
        projects = projectService.findAllDto();
        Assert.assertEquals(1, projects.size());
        Assert.assertEquals(projectOne, projects.get(0));
    }

    @Test
    public void testPersistCollection() {
        @Nullable final List<ProjectDto> nullCollection = null;
        projectService.persist(nullCollection);
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertTrue(projects.isEmpty());

        projectService.persist(allProjects);
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

    @Test
    public void testPersistVarargs() {
        projectService.persist(projectOne, projectTwo, projectThree);
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

    @Test
    public void testPersistVarargsWithNull() {
        projectService.persist(null, projectThree, null);
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(1, projects.size());

    }

    @Test
    public void testMerge() throws EmptyIdException {
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectOne.setName(newName);
        projectOne.setDescription(newDescription);

        projectService.merge(projectOne);
        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project, projectOne);
    }

    @Test
    public void testMergeCollection() {
        @Nullable final List<ProjectDto> nullCollection = null;
        projectService.persist(nullCollection);
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertTrue(projects.isEmpty());

        loadData();
        @NotNull final String newName = "new name1";
        @NotNull final String newDescription = "new description1";
        for (@NotNull final ProjectDto project : allProjects) {
            project.setName(newName);
            project.setDescription(newDescription);
        }

        projectService.merge(allProjects);
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
        for (@NotNull final ProjectDto project : projects) {
            Assert.assertEquals(project.getName(), newName);
            Assert.assertEquals(project.getDescription(), newDescription);
        }
    }

    @Test
    public void testMergeVarargs() {
        loadData();
        @NotNull final String newName = "new name2";
        @NotNull final String newDescription = "new description2";
        for (@NotNull final ProjectDto project : allProjects) {
            project.setName(newName);
            project.setDescription(newDescription);
        }

        projectService.merge(projectOne, projectTwo, projectThree);
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
        for (@NotNull final ProjectDto project : projects) {
            Assert.assertEquals(project.getName(), newName);
            Assert.assertEquals(project.getDescription(), newDescription);
        }
    }

    @Test
    public void testMergeVarargsWithNull() throws EmptyIdException {
        loadData();
        @NotNull final String newName = "new name3";
        @NotNull final String newDescription = "new description3";
        projectTwo.setName(newName);
        projectTwo.setDescription(newDescription);

        projectService.merge(null, projectTwo, null);
        @Nullable final ProjectDto project = projectService.findOneDtoById(projectTwo.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project, projectTwo);
    }

    @Test
    public void testRemoveOne() throws EmptyIdException {
        loadData();

        @Nullable final ProjectDto nullProject = null;
        projectService.removeOne(nullProject);
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());

        projectService.removeOne(projectOne);
        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNull(project);
    }

}
