package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.*;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyDomainException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class DomainServiceTest extends AbstractDataTest {

    @NotNull
    private final IDomainService domainService = bootstrap.getDomainService();

    @NotNull
    private final List<UserDto> allUserInSystem = new ArrayList<>(Arrays.asList(userDto, adminDto));

    @NotNull
    private final ProjectDto projectOne = new ProjectDto(
            "Project2",
            "",
            userDto.getId()
    );

    @NotNull
    private final ProjectDto projectTwo = new ProjectDto(
            "Project3",
            null,
            adminDto.getId()
    );

    @NotNull
    private final List<ProjectDto> allProjectInSystem = new ArrayList<>(Arrays.asList(projectOne, projectTwo));

    @NotNull
    private final TaskDto taskOne = new TaskDto(
            "Task2",
            null,
            userDto.getId()
    );

    @NotNull
    private final TaskDto taskTwo = new TaskDto(
            "Task3",
            null,
            adminDto.getId()
    );

    @NotNull
    private final List<TaskDto> allTaskInSystem = new ArrayList<>(Arrays.asList(taskOne, taskTwo));

    @NotNull
    private final SessionDto sessionOne = bootstrap.getSessionService().setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    userDto.getId(),
                    null
            )
    );

    @NotNull
    private final SessionDto sessionTwo = bootstrap.getSessionService().setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    adminDto.getId(),
                    null
            )
    );

    @NotNull
    private final List<SessionDto> allSessionInSystem = new ArrayList<>(Arrays.asList(sessionOne, sessionTwo));

    private void loadData() {
        bootstrap.getUserService().persist(allUserInSystem);
        bootstrap.getProjectService().persist(allProjectInSystem);
        bootstrap.getSessionService().persist(allSessionInSystem);
        bootstrap.getTaskService().persist(allTaskInSystem);
    }

    public DomainServiceTest() throws Exception {
        super();
    }

    @After
    public void after() {
        bootstrap.getTaskService().removeAll();
        bootstrap.getProjectService().removeAll();
        bootstrap.getSessionService().removeAll();
        bootstrap.getUserService().removeOne(userDto);
        bootstrap.getUserService().removeOne(adminDto);
    }


    @Test
    public void testSaveData() throws EmptyDomainException {
        loadData();
        @NotNull final Domain domain = new Domain();

        domainService.saveData(domain);

        @NotNull final List<ProjectDto> projects = domain.getProjects();
        Assert.assertEquals(2, projects.size());
        for (@NotNull final ProjectDto project: projects)
            Assert.assertTrue(allProjectInSystem.contains(project));

        @NotNull final List<TaskDto> tasks = domain.getTasks();
        Assert.assertEquals(2, tasks.size());
        for (@NotNull final TaskDto task: tasks)
            Assert.assertTrue(allTaskInSystem.contains(task));

        @NotNull final List<SessionDto> sessions = domain.getSessions();
        Assert.assertEquals(2, sessions.size());
        for (@NotNull final SessionDto session: sessions)
            Assert.assertTrue(allSessionInSystem.contains(session));

        @NotNull final List<UserDto> users = domain.getUsers();
        Assert.assertEquals(4, users.size());
    }

    @Test(expected = EmptyDomainException.class)
    public void testSaveDataWithoutDomain() throws EmptyDomainException {
        domainService.saveData(null);
    }

    @Test
    public void testLoadData() throws AbstractException {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(allProjectInSystem);
        domain.setTasks(allTaskInSystem);
        domain.setUsers(allUserInSystem);
        domain.setSessions(allSessionInSystem);

        domainService.loadData(domain);
        @NotNull final List<ProjectDto> projects = bootstrap.getProjectService().findAllDto();
        Assert.assertEquals(2, projects.size());
        for (@NotNull final ProjectDto project: projects)
            Assert.assertTrue(allProjectInSystem.contains(project));

        @NotNull final List<TaskDto> tasks = bootstrap.getTaskService().findAllDto();
        Assert.assertEquals(2, tasks.size());
        for (@NotNull final TaskDto task: tasks)
            Assert.assertTrue(allTaskInSystem.contains(task));

        @NotNull final List<SessionDto> sessions = bootstrap.getSessionService().findAllDto();
        Assert.assertEquals(2, sessions.size());
        for (@NotNull final SessionDto session: sessions)
            Assert.assertTrue(allSessionInSystem.contains(session));

        @NotNull final List<UserDto> users = bootstrap.getUserService().findAllDto();
        Assert.assertEquals(4, users.size());
    }

    @Test(expected = EmptyDomainException.class)
    public void testLoadDataWithoutDomain() throws EmptyDomainException {
        domainService.loadData(null);
    }

}
