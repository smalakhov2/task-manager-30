package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import ru.malakhov.tm.bootstrap.Bootstrap;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.enumerated.Role;

public abstract class AbstractDataTest {

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    protected final String USER_PASSWORD = "test1";

    protected final String ADMIN_PASSWORD = "admin1";

    @NotNull
    protected final UserDto userDto = new UserDto("test1", "test1", "3@1.ru");

    @NotNull
    protected final UserDto adminDto = new UserDto("admin1", "admin1", "4@2.ru");

    @NotNull
    protected final UserDto unknownUserDto = new UserDto("unknown", "unknown", "2@2.ru");

    @BeforeClass
    public static void beforeClass() throws Exception {
        bootstrap.getPropertyService().init();
        bootstrap.getSqlSessionService().initFactory();
    }

    @AfterClass
    public static void afterClass() {
        bootstrap.getSqlSessionService().closeFactory();
    }

    public AbstractDataTest() throws Exception {
        adminDto.setRole(Role.ADMIN);
    }

}