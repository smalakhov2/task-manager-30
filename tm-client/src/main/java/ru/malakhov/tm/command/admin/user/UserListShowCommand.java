package ru.malakhov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.UserDto;

import java.util.List;

public final class UserListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user list.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[USER-LIST]");
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @NotNull final List<UserDto> users = serviceLocator.getAdminUserEndpoint().getAllUserList(session);
        int index = 1;
        for (@Nullable final UserDto user : users) {
            if (user != null) {
                System.out.println(index + ". " + user.getLogin());
                index++;
            }
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}