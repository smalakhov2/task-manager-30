package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.ServerDto;
import ru.malakhov.tm.endpoint.SessionDto;

public class ServerInfoShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show server info.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @NotNull final ServerDto server = serviceLocator.getAdminDataEndpoint().getServerInfo(session);
        System.out.println("[SERVER-INFO]");
        System.out.println("SERVER HOST: " + server.getHost());
        System.out.println("SERVER PORT: " + server.getPort());
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }
}
