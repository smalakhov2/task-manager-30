package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.Map;
import java.util.Set;

public interface ICommandRepository {

    void add(@NotNull String name, @NotNull AbstractCommand command);

    @NotNull
    Map<String, AbstractCommand> getCommands();

    @NotNull
    Set<String> getCommandsName();

    @NotNull
    Set<String> getCommandsArg();

}