package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.category.IntegrationCategory;

import java.util.List;

@Category(IntegrationCategory.class)
public final class AdminUserEndpointTest extends AbstractIntegrationTest{

    @NotNull
    private final AdminUserEndpoint auEndpoint = new AdminUserEndpointService().getAdminUserEndpointPort();

    @NotNull final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    private static final String TEST_USER_LOGIN = "login";

    private static final String TEST_USER_PASSWORD = "password";

    private void createTestUser() throws AbstractException_Exception {
        userEndpoint.registryUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, "1@1.ru");
    }

    private void removeTestUser() throws AbstractException_Exception {
        auEndpoint.removeUserByLogin(adminSession, TEST_USER_LOGIN);
    }

    @Test
    public void testGetUserList() throws AbstractException_Exception {
        @NotNull final List<UserDto> users = auEndpoint.getAllUserList(adminSession);
        Assert.assertEquals(2, users.size());
    }

    @Test
    public void testLockUserByLogin() throws AbstractException_Exception {
        createTestUser();
        @NotNull final Result result = auEndpoint.lockUserByLogin(adminSession, TEST_USER_LOGIN);
        Assert.assertTrue(result.success);
        @NotNull final SessionDto session = sessionEndpoint.openSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        Assert.assertNull(session);
        removeTestUser();
    }

    @Test
    public void testUnlockUserByLogin() throws AbstractException_Exception {
        createTestUser();
        auEndpoint.lockUserByLogin(adminSession, TEST_USER_LOGIN);
        auEndpoint.unlockUserByLogin(adminSession, TEST_USER_LOGIN);
        @NotNull final SessionDto session = sessionEndpoint.openSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
        removeTestUser();
    }

    @Test
    public void removeUserByLogin() throws AbstractException_Exception {
        createTestUser();
        auEndpoint.removeUserByLogin(adminSession, TEST_USER_LOGIN);
        @NotNull final List<UserDto> users = auEndpoint.getAllUserList(adminSession);
        Assert.assertEquals(2, users.size());
    }

}
